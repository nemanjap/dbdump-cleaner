from dateutil.relativedelta import relativedelta
import datetime
import logging
import json
import re
import os
import random
import calendar
import shutil

def check_dump_location(dump_location):
    """ Checks if the location provided is really the location where db-dumps are stored """
    # Check if the provided path is a directory
    if not os.path.isdir(dump_location):
        raise ValueError('Provided path is not a directory.')
    # Get the listing of all directories that are located in the provided directory
    directory_listing = [directory for directory in os.listdir(dump_location) if
            os.path.isdir(os.path.join(dump_location, directory))]
    pattern = re.compile("\d{2}")
    for item in directory_listing:
        if not pattern.match(str(item)):
            raise ValueError('Provided location contains a dir that does not follow the naming convention - {}'.format(item))
            return False
    return True

def configure_logging():
    """ Configure basic logging parameters """
    logging.basicConfig(filename ='/var/log/portal-dbdump-cleaner.log', filemode='w', format='%(asctime)s - %(levelname)s - %(message)s',
            datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)

def get_json_properties(properties_file):
    """ Returns JSON properties from a local JSON file"""
    with open(properties_file) as json_properties_file:
        return json.load(json_properties_file)

def get_paths(dump_location):
    """ Grabs all paths from the provided locaition that might containe files to be cleaned """
    paths = {}
    year_listing = [directory for directory in os.listdir(dump_location) if os.path.isdir(os.path.join(dump_location, directory))]
    for year_dir in year_listing:
        year_path = os.path.join(dump_location, year_dir)
        month_listing = [directory for directory in os.listdir(year_path) if os.path.isdir(os.path.join(year_path, directory))]
        for month_dir in month_listing:
            month_path = os.path.join(year_path, month_dir)
            date_string = '20'+year_dir+'-'+month_dir
            date = datetime.datetime.strptime(date_string, '%Y-%m').date()
            paths.update({month_path:date})
    return paths

def parse_paths(paths):
    """ Get paths that are years old, and get paths that are months old from the total paths that are provided """
    # Get current date/time 
    current_date = datetime.date.today().replace(day=1)
    # Get date differences
    years_old_date = current_date + relativedelta(years=-1)
    months_old_date = current_date + relativedelta(months=-3)
    # Debug line
    logging.debug('Current date is: {}, Years old date is: {}, and months old date is: {}'.format(current_date,
        years_old_date, months_old_date))
    # Helper variables for holding path entries
    years_old_paths = {}
    months_old_paths = {}
    # If a date is older than a year place it in years_old list, and if it is beetwen one yar and 3 months old place in
    # in months_old list
    for key, value in paths.items():
        if value < years_old_date:
            years_old_paths[key]=value
        if value >= years_old_date and value <= months_old_date:
            months_old_paths[key]=value
    return years_old_paths, months_old_paths

def clean_months_old_paths(months_old_path):
    already_cleaned_filename = 'cleaned.month'
    for path, path_date in months_old_paths.items():
        # Returns weekday of first day of the month and number of days in month, for the specified year and month.
        month_range = calendar.monthrange(path_date.year,path_date.month)
        number_of_weeks = calculate_number_of_weeks(month_range)
        days_in_month = month_range[1]
        backup_days = calculate_backup_days(days_in_month, number_of_weeks)
        logging.debug('{} Checking if needs to be cleaned'.format(path))
        if not os.path.isfile(os.path.join(path, already_cleaned_filename)):
            logging.debug('{} - Marked for MONTH Cleanup'.format(path))
            days_listing = [directory for directory in os.listdir(path) if os.path.isdir(os.path.join(path, directory)) and int(directory) not in backup_days]
            logging.debug('{} -- Checking number of files'.format(path))
            if len(days_listing) <= 6:
                logging.debug('{} --- Less that 6 backup folders - SKIPPING'.format(path))
                open(os.path.join(path, already_cleaned_filename), 'a').close()
            else:
                logging.debug('{} {} --- will be kept'.format(path, backup_days))
                for day_dir in days_listing:
                    day_path = os.path.join(path, day_dir)
                    shutil.rmtree(day_path)
                logging.debug('{} ---- Cleaned.'.format(path))
                open(os.path.join(path, already_cleaned_filename), 'a').close()
        else:
            logging.debug('{} - Already clean.'.format(path))

def clean_years_old_paths(years_old_paths):
    already_cleaned_filename = 'cleaned.year'
    for path, path_date in years_old_paths.items():
        logging.debug('{} Checking if it needs to be cleaned'.format(path))
        if not os.path.isfile(os.path.join(path, already_cleaned_filename)):
            logging.debug('{} - Marked for YEAR cleanup'.format(path))
            days_listing = [directory for directory in os.listdir(path) if os.path.isdir(os.path.join(path, directory))]
            if len(days_listing) <= 1:
                logging.debug('{} -- Less than 1 backup folder - SKIPPING'.format(path))
                open(os.path.join(path, already_cleaned_filename), 'a').close()
            else:
                backup_directory = random.choice(days_listing)
                tmp_listing = list(days_listing)
                while len(tmp_listing) > 1 and len(os.listdir(os.path.join(path, backup_directory))) == 0:
                    tmp_listing.remove(backup_directory)
                    backup_directory = random.choice(tmp_listing)
                else:
                    if len(tmp_listing) == 1:
                        logging.error("{} All backup directories are empty - SKIPPING".format(path))
                        continue
                logging.debug('{} {} --- will be kept'.format(path, backup_directory))
                days_listing.remove(backup_directory)
                for day_dir in days_listing:
                    shutil.rmtree(os.path.join(path, day_dir))
                logging.debug('{} -- Cleaned.'.format(path))
                open(os.path.join(path, already_cleaned_filename), 'a').close()
        else:
            logging.debug('{} - Already clean.'.format(path))

def calculate_backup_days(days_in_month, number_of_weeks):
    """ Get appropriate amount of days to be left in a Months backup folder """
    backup_days = []
    for day in range(1, days_in_month+1, 7):
        backup_days.append(day)
    if len(backup_days) < number_of_weeks:
        if days_in_month not in backup_days:
            backup_days.append(days_in_month)
    return backup_days

def calculate_number_of_weeks(month_range):
    """ Calculate if a month has 6 weeks, or five weeks """
    if (month_range[1]==30 and month_range[0]>=6) or (month_range[1]==31 and month_range[0]>=5):
        return 6
    return 5

""" Code to be run """
if __name__ == "__main__":
    try:
        """ Configure logging mechanism """
        configure_logging()
        logging.info('--------------- LOGGING STARTED ---------------')
        """ Load JSON properties from a JSON file """
        json_properties = get_json_properties('properties.json')
        logging.debug('Loaded JSON Properties file')
        dump_location = json_properties['dump_location']
        """ Check if the location provided in the JSON file is valid """
        check_dump_location(dump_location)
        """ Get all paths that contain files for given months """
        paths = get_paths(dump_location)
        logging.debug('Collected paths:\n{}'.format(sorted(paths.keys())))
        """ Get paths to be cleaned """
        years_old_paths, months_old_paths = parse_paths(paths)
        logging.debug('Parsed paths:\n\t\tYears old paths:\n{}\n\t\tMonths old paths:\n{}'.format(years_old_paths, months_old_paths))
        """ Clean paths of unneccessary files """
        """ Clean months_old_paths(months_old_paths) """
        clean_months_old_paths(months_old_paths)
        """Clean years old paths"""
        clean_years_old_paths(years_old_paths)
    except ValueError as err:
        logging.error(err)
        raise err
    except Exception as exc:
        logging.error(exc)
        raise exc
